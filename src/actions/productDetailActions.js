import * as Types from './../constants/ActionTypes';
export const fetchProductDetail = (product) => {
    return {
        type: Types.FETCH_PRODUCT_DETAIL,
        product,
    }
}

export const toggleLikeProductDetail = (type_toggle) => {
    return {
        type: Types.TOGGLE_LIKE_PRODUCT_DETAIL,
        type_toggle
    }
}