import * as Types from './../constants/ActionTypes';
export const fetchMyProducts = (products) => {
    return {
        type: Types.FETCH_MYPRODUCTS,
        products
    }
}

export const updateStatusMyProducts = (id, status) => {
    return {
        type: Types.UPDATE_STATUS_MYPRODUCTS,
        id,
        status
    }
}

