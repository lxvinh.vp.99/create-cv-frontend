import * as Types from './../constants/ActionTypes';
export const fetchProducts = (totalProducts, products) => {
    return {
        type: Types.FETCH_PRODUCTS,
        totalProducts,
        products
    }
}
export const toggleLikeProduct = (index, type_toggle) => {
    return {
        type: Types.TOGGLE_LIKE_PRODUCT,
        index,
        type_toggle
    }
}