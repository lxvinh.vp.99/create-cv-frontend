import React from 'react';
import withAuth from '../../components/withAuth/withAuth';
import WishListFrame from '../../components/WishListFrame/WishListFrame';
import { ToastContainer, Slide } from 'react-toastify';
const CartPage = (props) => {
    return (
        <React.Fragment>
            <ToastContainer
                style={{ top: 56 + "px" }}
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                transition={Slide}
            />
            <div className="container wishlist-wrapper">
                <WishListFrame></WishListFrame>
            </div>
        </React.Fragment>
    )
}
export default withAuth(CartPage);
