import React, { useState } from 'react';
import withAuth from '../../components/withAuth/withAuth';
import ProductDetailFrame from '../../components/ProductDetailFrame/ProductDetailFrame';
import { ToastContainer, Slide } from 'react-toastify';
const ProductDetailPage = (props) => {
    return (
        <React.Fragment>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                transition={Slide}
            />
            <ProductDetailFrame {...props}></ProductDetailFrame>
        </React.Fragment>
    )
}
export default withAuth(ProductDetailPage);
