import React, { useEffect } from 'react';
import withAuth from '../../components/withAuth/withAuth';
import Loader from '../../components/Loader/Loader';
const Header = () => {
  return (
    <React.Fragment>
      <Loader></Loader>
    </React.Fragment>
  )
}
export default withAuth(Header);
