import React, { useEffect } from 'react';
import SignUpForm from '../../components/SignUpForm/SignUpForm';
import { ToastContainer, Zoom } from 'react-toastify';
import withAuth from '../../components/withAuth/withAuth';
const SignUpPage = (props) => {
    return (
        <React.Fragment>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                transition={Zoom}
            />
            <div className="auth-wrapper">
                <div className="auth-inner">
                    <SignUpForm {...props}></SignUpForm>
                </div>
            </div>
        </React.Fragment>
    )
}
export default withAuth(SignUpPage);
