import * as Types from './../constants/ActionTypes';
import _ from 'lodash';
let initialState = [];
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.FETCH_MYPRODUCTS:
            state = action.products;
            return [...state];
        case Types.UPDATE_STATUS_MYPRODUCTS:
            let index = _.findIndex(state, function (product) { return product._id == action.id; });
            state[index].status = action.status;
            return [...state];
        default:
            return [...state];
    }
};

export default myReducer;