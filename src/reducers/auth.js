import * as Types from './../constants/ActionTypes';
import _ from 'lodash';
let initialState = {
    isLogin: false,
    userInfo: {}
};
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.AUTH_USER:
            // console.log(action.isLogin);
            state.isLogin = action.isLogin;
            state.userInfo = action.userInfo;
            return { ...state };
        case Types.EDIT_USER:
            state.userInfo = action.userInfo;
            return { ...state };
        case Types.TOGGLE_LIKE_PRODUCT:
            action.type_toggle == "add" ? state.userInfo.totalWishProducts++ : state.userInfo.totalWishProducts--;
            return { ...state };
        case Types.TOGGLE_LIKE_PRODUCT_DETAIL:
            action.type_toggle == "add" ? state.userInfo.totalWishProducts++ : state.userInfo.totalWishProducts--;
            return { ...state };
        case Types.REMOVE_WISH_PRODUCT:
            state.userInfo.totalWishProducts--;
            return { ...state };
        default:
            return { ...state };
    }
};

export default myReducer;