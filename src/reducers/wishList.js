import * as Types from './../constants/ActionTypes';
import _ from 'lodash';
let initialState = [];
const myReducer = (state = initialState, action) => {
    switch (action.type) {
        case Types.FETCH_WISHLIST:
            state = action.products;
            return [...state];
        case Types.REMOVE_WISH_PRODUCT:
            _.remove(state, function (product) {
                return product._id == action.id;
            });
            return [...state];
        default:
            return [...state];
    }
};

export default myReducer;