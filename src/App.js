import React, { useState, useEffect } from 'react';
// import logo from './logo.svg';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import routes from './routes';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';
function App() {
  return (
    <Router>
      <ScrollToTop></ScrollToTop>
      <Header></Header>
      <Switch>
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Switch>
      <Footer></Footer>
    </Router>
  );
}

function RouteWithSubRoutes(route) {
  return (
    <Route
      isPrivate={route.isPrivate}
      path={route.path}
      exact={route.exact}
      component={route.main}
    />
  );
}
export default App;
