import React, { useState, useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { Form, FormGroup, Label, Input, Row, Col, Button } from 'reactstrap';
import { useSelector } from 'react-redux';
// import _ from 'lodash';
import callApi from '../../helpers/apiCaller';
import Swal from 'sweetalert2';
import * as authActions from '../../actions/authActions';
import './style.scss';
function useStateCallback(initialState) {
  const [state, setState] = useState(initialState);
  const cbRef = useRef(null);

  const setStateCallback = (state, cb) => {
    cbRef.current = cb;
    setState(state);
  };
  useEffect(() => {
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = null;
    }
  }, [state]);
  return [state, setStateCallback];
}
const ChangeInfoForm = () => {
  const auth = useSelector(state => state.auth);
  const [info, setInfo] = useState({});
  const [isDisplayLoading, setIsDisplayLoading] = useStateCallback(false);
  const dispatch = useDispatch();
  useEffect(() => {
    setInfo(auth.userInfo);
  }, [auth.userInfo]);
  const onChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setInfo({ ...info, [name]: value });
  }
  const onSave = () => {
    setIsDisplayLoading(true, () => {
      callApi('users/editprofile', 'POST', info).then(res => {
        setTimeout(() => {
          setIsDisplayLoading(false);
          Swal.fire(
            'Thành công',
            'Thông tin của bạn đã được cập nhật',
            'success'
          );
          dispatch(authActions.editUser(info));
        }, 500);
      }).catch(err => {
        setTimeout(() => {
          setIsDisplayLoading(false);
          if (err && err.response) {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: `${err.response.data.message}`,
            });
          }
          else {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Có lỗi xảy ra vui lòng thử lại...',
            });
          }
        }, 500);
      });
    });
  }
  return (
    <div className="row myinfo">
      <div className="col-sm-12">
        <strong className="profile-title"> Profile Details</strong>
        <hr />
      </div>
      <div className="col-xs-12 col-lg-12">
        <Form>
          <Row form>
            <Col xs={12} md={6}>
              <FormGroup>
                <Label for="firstName">First Name</Label>
                <Input id="firstName" value={info && info.firstName ? info.firstName : ""} name="firstName" onChange={onChange} />
              </FormGroup>
            </Col>
            <Col xs={12} md={6}>
              <FormGroup>
                <Label for="lastName">Last Name</Label>
                <Input id="lastName" value={info && info.lastName ? info.lastName : ""} name="lastName" onChange={onChange} />
              </FormGroup>
            </Col>
            <Col xs={12} md={6}>
              <FormGroup>
                <Label for="email">Email</Label>
                <Input type="email" id="email" value={info && info.email ? info.email : ""} name="email" disabled />
              </FormGroup>
            </Col>
            <Col xs={12} md={6}>
              <FormGroup>
                <Label for="address">Address</Label>
                <Input id="address" name="address" value={info && info.address ? info.address : ""} onChange={onChange} />
              </FormGroup>
            </Col>
            <Col xs={12} md={6}>
              <FormGroup>
                <Label for="phone">Phone Number</Label>
                <Input type="tel" name="phoneNumber" value={info && info.phoneNumber ? info.phoneNumber : ""} onChange={onChange} />
              </FormGroup>
            </Col>
          </Row>
        </Form>
        <hr />
        <div className="savechanges">
          <Button className="save" onClick={onSave}>Save Changes</Button>
        </div>
      </div>
    </div>
  )
}
export default ChangeInfoForm;
