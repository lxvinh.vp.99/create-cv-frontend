import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import { Button, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, InputGroupText, Col, Row, ButtonGroup } from 'reactstrap';
import { toast } from 'react-toastify';
function useStateCallback(initialState) {
  const [state, setState] = useState(initialState);
  const cbRef = useRef(null);

  const setStateCallback = (state, cb) => {
    cbRef.current = cb;
    setState(state);
  };
  useEffect(() => {
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = null;
    }
  }, [state]);
  return [state, setStateCallback];
}
const PostSaleForm = () => {
  const [imagePreviewUrls, setImagePreviewUrls] = useState([]);
  const [files, setFiles] = useState([]);
  const [CVInfo, setCVInfo] = useState({});
  const [CVTarget, setCVTarget] = useState("");
  const [CVHooby, setCVHooby] = useState("");
  const [education, setEducation] = useState([{}]);
  const [experience, setExperience] = useState([{}]);
  const [skills, setSkills] = useState([{}]);
  const onAddImages = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const files = Array.from(e.target.files);
      Promise.all(files.map(file => {
        setFiles()
        return (new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.addEventListener('load', (ev) => {
            resolve(ev.target.result);
          });
          reader.addEventListener('error', reject);
          reader.readAsDataURL(file);
        }));
      }))
        .then(images => {
          setImagePreviewUrls(images);
          setFiles(files);
        }, error => {
          console.error(error);
        });
    }
  }

  const onSubmitForm = (e) => {
    e.preventDefault();
    const formData = new FormData();
    const payload = {
      cv_info: CVInfo,
      cv_target: CVTarget,
      cv_hooby: CVHooby,
      cv_edu: education,
      cv_exp: experience,
      cv_skills: skills,
    };
    formData.append("cv_avatar", files[0]);
    for (const property in payload) {
      formData.append(property, JSON.stringify(payload[property]));
    }
    callApi('cvs/create', 'POST', formData).then(res => {
      const data = res.data.data;
      const a = document.createElement('a');
      a.href = `data:application/octet-stream;base64,${data}`;
      a.download = "cv.pdf";
      a.click();
      toast.dismiss();
      toast.success("👌 Lưu thông tin thành công");
    }).catch(err => {
      toast.dismiss();
      toast.error(`👌 ${err.response.data.message}`);
    });
  }

  let elmImages = [];
  if (imagePreviewUrls.length > 0) {
    elmImages = imagePreviewUrls.map((url, index) => {
      return <div key={index + 1} index={index + 1} style={{ backgroundImage: `url(${url})`, backgroundSize: "100% 100%" }} className="select-images" />
    });
  }

  const handleChangeEducation = (e, index) => {
    const { name, value } = e.target;
    setEducation(preEdu => {
      preEdu[index][name] = value;
      return [...preEdu];
    });
  }

  const handleDeleteEdu = (e, index) => {
    e.preventDefault();
    setEducation(preEdu => {
      preEdu.splice(index, 1);
      return [...preEdu];
    });
  }

  const handleAddEdu = (e, index) => {
    e.preventDefault();
    const item = {};
    setEducation(preEdu => {
      preEdu.splice(index + 1, 0, item);
      return [...preEdu];
    });
  }

  const handleUpEdu = (e, index) => {
    e.preventDefault();
    if (index != 0) {
      setEducation(preEdu => {
        let temp = preEdu[index - 1];
        preEdu[index - 1] = preEdu[index];
        preEdu[index] = temp;
        return [...preEdu];
      });
    }
  }

  const handleDownEdu = (e, index) => {
    e.preventDefault();
    if (index != education.length - 1) {
      setEducation(preEdu => {
        let temp = preEdu[index + 1];
        preEdu[index + 1] = preEdu[index];
        preEdu[index] = temp;
        return [...preEdu];
      });
    }
  }

  const handleChangeExperience = (e, index) => {
    const { name, value } = e.target;
    setExperience(preExp => {
      preExp[index][name] = value;
      return [...preExp];
    });
  }

  const handleDeleteExp = (e, index) => {
    e.preventDefault();
    setExperience(preExp => {
      preExp.splice(index, 1);
      return [...preExp];
    });
  }

  const handleAddExp = (e, index) => {
    e.preventDefault();
    const item = {};
    setExperience(preExp => {
      preExp.splice(index + 1, 0, item);
      return [...preExp];
    });
  }

  const handleUpExp = (e, index) => {
    e.preventDefault();
    if (index != 0) {
      setExperience(preExp => {
        let temp = preExp[index - 1];
        preExp[index - 1] = preExp[index];
        preExp[index] = temp;
        return [...preExp];
      });
    }
  }

  const handleDownExp = (e, index) => {
    e.preventDefault();
    if (index != experience.length - 1) {
      setExperience(preExp => {
        let temp = preExp[index + 1];
        preExp[index + 1] = preExp[index];
        preExp[index] = temp;
        return [...preExp];
      });
    }
  }

  const renderEducation = () => {
    return education.map((edu, index) => {
      return (<FormGroup key={index} style={{ position: "relative", marginBottom: "40px" }}>
        <div style={{ position: "absolute", top: "-32px", right: "0" }}>
          <Button size="sm" color="secondary" onClick={(e) => handleUpEdu(e, index)}><i className="fas fa-caret-up"></i></Button>
          <Button size="sm" color="secondary" onClick={(e) => handleDownEdu(e, index)} style={{ marginLeft: "5px" }}><i className="fas fa-caret-down"></i></Button>
          <Button size="sm" color="primary" onClick={(e) => handleAddEdu(e, index)} style={{ marginLeft: "5px" }}>+ Thêm</Button>
          {education.length > 1 && <Button size="sm" color="danger" onClick={(e) => handleDeleteEdu(e, index)} style={{ marginLeft: "5px" }}>- Xóa</Button>}
        </div>
        <Row form style={{ marginBottom: "5px" }}>
          <Col md={8}>
            <Input type="text" name="school" placeholder="TÊN TRƯỜNG HỌC" value={edu.school || ""} onChange={e => handleChangeEducation(e, index)} />
          </Col>
          <Col md={4}>
            <Row form style={{ justifyContent: "flex-end", alignItems: "center" }}>
              <Col md={5}>
                <Input type="text" name="start" placeholder="BẮT ĐẦU" value={edu.start || ""} onChange={e => handleChangeEducation(e, index)} />
              </Col>
              <Col md={1}>-</Col>
              <Col md={5}>
                <Input type="text" name="end" placeholder="KẾT THÚC" value={edu.end || ""} onChange={e => handleChangeEducation(e, index)} />
              </Col>
            </Row>
          </Col>
        </Row>
        <Input type="text" name="falcuty" placeholder="Ngành học / Môn học" value={edu.falcuty || ""} style={{ marginBottom: "5px" }} onChange={e => handleChangeEducation(e, index)} />
        <Input type="textarea" name="description" placeholder="Mô tả chi tiết" id="description" value={edu.description || ""} onChange={e => handleChangeEducation(e, index)} />
      </FormGroup>);
    })
  }

  const renderExperience = () => {
    return experience.map((exp, index) => {
      return (<FormGroup key={index} style={{ position: "relative", marginBottom: "40px" }}>
        <div style={{ position: "absolute", top: "-32px", right: "0" }}>
          <Button size="sm" color="secondary" onClick={(e) => handleUpExp(e, index)}><i className="fas fa-caret-up"></i></Button>
          <Button size="sm" color="secondary" onClick={(e) => handleDownExp(e, index)} style={{ marginLeft: "5px" }}><i className="fas fa-caret-down"></i></Button>
          <Button size="sm" color="primary" onClick={(e) => handleAddExp(e, index)} style={{ marginLeft: "5px" }}>+ Thêm</Button>
          {experience.length > 1 && <Button size="sm" color="danger" onClick={(e) => handleDeleteExp(e, index)} style={{ marginLeft: "5px" }}>- Xóa</Button>}
        </div>
        <Row form style={{ marginBottom: "5px" }}>
          <Col md={8}>
            <Input type="text" name="company" placeholder="TÊN CÔNG TY" value={exp.company || ""} onChange={e => handleChangeExperience(e, index)} />
          </Col>
          <Col md={4}>
            <Row form style={{ justifyContent: "flex-end", alignItems: "center" }}>
              <Col md={5}>
                <Input type="text" name="start" placeholder="BẮT ĐẦU" value={exp.start || ""} onChange={e => handleChangeExperience(e, index)} />
              </Col>
              <Col md={1}>-</Col>
              <Col md={5}>
                <Input type="text" name="end" placeholder="KẾT THÚC" value={exp.end || ""} onChange={e => handleChangeExperience(e, index)} />
              </Col>
            </Row>
          </Col>
        </Row>
        <Input type="text" name="position" placeholder="Vị trí công việc" value={exp.position || ""} style={{ marginBottom: "5px" }} onChange={e => handleChangeExperience(e, index)} />
        <Input type="textarea" name="description" placeholder="Mô tả chi tiết công việc, những gì đạt được trong quá trình làm việc." id="description" value={exp.description || ""} onChange={e => handleChangeExperience(e, index)} />
      </FormGroup>);
    })
  }

  const handleChangeSkill = (e, index) => {
    const { name, value } = e.target;
    setSkills(preSkills => {
      preSkills[index][name] = value;
      return [...preSkills];
    });
  }

  const handleDeleteSkill = (e, index) => {
    e.preventDefault();
    setSkills(preExp => {
      preExp.splice(index, 1);
      return [...preExp];
    });
  }

  const handleAddSkill = (e, index) => {
    e.preventDefault();
    const item = {};
    setSkills(preExp => {
      preExp.splice(index + 1, 0, item);
      return [...preExp];
    });
  }

  const handleUpSkill = (e, index) => {
    e.preventDefault();
    if (index != 0) {
      setSkills(preExp => {
        let temp = preExp[index - 1];
        preExp[index - 1] = preExp[index];
        preExp[index] = temp;
        return [...preExp];
      });
    }
  }

  const handleDownSkill = (e, index) => {
    e.preventDefault();
    if (index != skills.length - 1) {
      setSkills(preExp => {
        let temp = preExp[index + 1];
        preExp[index + 1] = preExp[index];
        preExp[index] = temp;
        return [...preExp];
      });
    }
  }

  const renderSkills = () => {
    return skills.map((skill, index) => {
      return (
        <FormGroup key={index} row style={{ position: "relative", marginBottom: "40px" }}>
          <div style={{ position: "absolute", top: "-32px", right: "0" }}>
            <Button size="sm" color="secondary" onClick={(e) => handleUpSkill(e, index)}><i className="fas fa-caret-up"></i></Button>
            <Button size="sm" color="secondary" onClick={(e) => handleDownSkill(e, index)} style={{ marginLeft: "5px" }}><i className="fas fa-caret-down"></i></Button>
            <Button size="sm" color="primary" onClick={(e) => handleAddSkill(e, index)} style={{ marginLeft: "5px" }}>+ Thêm</Button>
            {skills.length > 1 && <Button size="sm" color="danger" onClick={(e) => handleDeleteSkill(e, index)} style={{ marginLeft: "5px" }}>- Xóa</Button>}
          </div>
          <Col sm={4}>
            <Input type="text" name="name" value={skill.name || ""} onChange={e => handleChangeSkill(e, index)} />
          </Col>
          <Col sm={2}>
            <Input type="text" disabled value={`${skill.percent || 0}%`} />
          </Col>
          <Col sm={6}>
            <input type="range" name="percent" value={skill.percent || 0} style={{ width: "100%" }} step={1} onChange={e => handleChangeSkill(e, index)} />
          </Col>
        </FormGroup>
      )
    })
  }

  const handleChangeCVInfo = (e) => {
    const { value, name } = e.target;
    setCVInfo({
      ...CVInfo,
      [name]: value
    })
  }

  const handleChangeCVTarget = (e) => {
    const { value } = e.target;
    setCVTarget(value);
  }

  const handleChangeCVHooby = (e) => {
    const { value } = e.target;
    setCVHooby(value);
  }

  return (
    <React.Fragment>
      <div className="container" style={{ marginTop: 20 + "px", marginBottom: 20 + "px" }}>
        <Form onSubmit={onSubmitForm}>
          <div className="row">
            <div className="col-12 col-sm-12 col-md-4">
              <h2><i className="fas fa-info-circle"></i> Thông tin cá nhân</h2>
              <div className="images-wrapper">
                {elmImages}
                <label htmlFor="files" className="select-images">
                  <i className="fas fa-camera" style={{ width: 50 + "%", height: 50 + "%", transform: "translate(50%, 50%)", color: "orange" }}></i>
                  <i className="fas fa-plus" style={{ color: "orange", position: "absolute", right: 5 + "px", top: 5 + "px" }}></i>
                </label>
                <input type="file" id="files" name="files" style={{ display: "none" }} onChange={onAddImages} />
              </div>
              <FormGroup>
                <Input type="text" name="cv_name" placeholder="Họ tên" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_position" placeholder="Vị trí công việc bạn muốn ứng tuyển" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_birthday" placeholder="Ngày sinh" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_sex" placeholder="Giới tính" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_phone" placeholder="Số điện thoại" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_email" placeholder="Địa chỉ email" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_address" placeholder="Địa chỉ hiện tại" onChange={handleChangeCVInfo} />
              </FormGroup>
              <FormGroup>
                <Input type="text" name="cv_website" placeholder="Website, Facebook, Blog,..." onChange={handleChangeCVInfo} />
              </FormGroup>
              <h2>Mục tiêu nghề nghiệp</h2>
              <FormGroup>
                <Input type="textarea" name="cv_target" placeholder="Mục tiêu nghề nghiệp: ngắn hạn, dài han." onChange={handleChangeCVTarget} />
              </FormGroup>
              <h2>Sở thích</h2>
              <FormGroup>
                <Input type="textarea" name="cv_hooby" placeholder="Sở thích" onChange={handleChangeCVHooby} />
              </FormGroup>
            </div>
            <div className="col-12 col-sm-12 col-md-8">
              <h2><i className="fas fa-graduation-cap"></i> Học vấn</h2>
              {renderEducation()}
              <h2><i className="fas fa-briefcase"></i> Kinh nghiệm</h2>
              {renderExperience()}
              <h2><i className="fas fa-briefcase"></i> Kỹ năng</h2>
              {renderSkills()}
            </div>
          </div>
          <Button color="success" style={{ margin: "auto", display: "block" }}>Lưu CV</Button>
        </Form>
      </div>
    </React.Fragment>
  )
}
export default PostSaleForm;
