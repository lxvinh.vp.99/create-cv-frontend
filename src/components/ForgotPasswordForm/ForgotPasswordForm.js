import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import callApi from '../../helpers/apiCaller';
import { toast } from 'react-toastify';
const ForgotPasswordForm = (props) => {
    const [email, setEmail] = useState("");
    const [isProcess, setIsProcess] = useState(false);
    const onChange = (e) => {
        let value = e.target.value;
        setEmail(value);
    }
    const onForgot = (e) => {
        e.preventDefault();
        setIsProcess(true);
        callApi('users/forgotpassword', 'POST', { email: email }).then(res => {
            toast.dismiss();
            toast.success(`${res.data.response.msg}`);
            setEmail("");
        }).catch(err => {
            toast.dismiss();
            toast.error(`${err.response.data.message}`);
        }).finally(() => {
            setIsProcess(false);
        });
    }
    return (
        <React.Fragment>
            <form>
                <h3>Forgot Password</h3>
                <div className="form-group">
                    <label>Email</label>
                    <input type="email"
                        className="form-control"
                        placeholder="Enter email"
                        value={email ? email : ""}
                        onChange={onChange}
                        name="email"
                    />
                </div>
                {isProcess ? <button className="btn btn-primary btn-block" type="button" disabled>
                    <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Vui lòng đợi...
                </button> : <button type="submit" className="btn btn-primary btn-block" onClick={onForgot}>Đặt lại mật khẩu</button>}
                <p className="forgot-password text-left" style={{ display: "flex", justifyContent: "space-between" }}>
                    <span>Already registered <Link to="/login">sign in?</Link></span>
                    <span>Create <Link to="/signup">new account</Link></span>
                </p>
            </form>
        </React.Fragment>
    )
}
export default ForgotPasswordForm;
