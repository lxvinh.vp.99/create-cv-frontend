import React from 'react';
import './style.scss';
const Loader = () => {
    return (
        <React.Fragment>
            <div className="loader-wrapper" style={{ height: 500 + "px" }}>
                <div className="circle-loader"></div>
                <div className="circle-small-loader"></div>
                <div className="circle-big-loader"></div>
                <div className="circle-inner-inner-loader"></div>
                <div className="circle-inner-loader"></div>
            </div>
        </React.Fragment>
    );
}
export default Loader;
