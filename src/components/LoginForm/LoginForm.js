import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import callApi from '../../helpers/apiCaller';
import { toast } from 'react-toastify';
const LoginForm = (props) => {
    const { state: stateFromOtherPage } = props.history.location;
    const [loginInfo, setLoginInfo] = useState({ email: "", password: "" });
    useEffect(() => {
        setLoginInfo({ email: stateFromOtherPage ? stateFromOtherPage.email : "", password: stateFromOtherPage ? stateFromOtherPage.password : "" });
    }, [stateFromOtherPage]);
    const onChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setLoginInfo({ ...loginInfo, [name]: value });
    }
    const onSignIn = (e) => {
        e.preventDefault();
        callApi('users/login', 'POST', loginInfo).then(res => {
            localStorage.setItem('token', res.data.data.token);
            window.location.href = "/";
        }).catch(err => {
            toast.dismiss();
            toast.error(`${err.response.data.message}`);
        });
    }
    return (
        <React.Fragment>
            <form>
                <h3>Sign In</h3>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email"
                        className="form-control"
                        placeholder="Enter email"
                        value={loginInfo && loginInfo.email ? loginInfo.email : ""}
                        onChange={onChange}
                        name="email"
                    />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password"
                        className="form-control"
                        placeholder="Enter password"
                        value={loginInfo && loginInfo.password ? loginInfo.password : ""}
                        onChange={onChange}
                        name="password"
                    />
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary btn-block" onClick={onSignIn}>Sign In</button>
                <p className="forgot-password text-left" style={{ display: "flex", justifyContent: "space-between" }}>
                    <span>Create <Link to="/signup">new account</Link></span>
                    <span>Forgot <Link to="/forgotpassword">password?</Link></span>
                </p>
            </form>
        </React.Fragment>
    )
}
export default LoginForm;
