import React, { useState, useEffect, useRef } from 'react';
import { Form, FormGroup, Label, Input, FormFeedback, Col, Button } from 'reactstrap';
import './style.scss';
import callApi from '../../helpers/apiCaller';
import Swal from 'sweetalert2';
function useStateCallback(initialState) {
  const [state, setState] = useState(initialState);
  const cbRef = useRef(null);
  const setStateCallback = (state, cb) => {
    cbRef.current = cb;
    setState(state);
  };
  useEffect(() => {
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = null;
    }
  }, [state]);
  return [state, setStateCallback];
}
const ChangePasswordForm = () => {
  const [payload, setPayload] = useState({});
  const [isDisplayLoading, setIsDisplayLoading] = useStateCallback(false);
  const onChange = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    setPayload({ ...payload, [name]: value });
  }
  const onChangePassword = () => {
    setIsDisplayLoading(true, () => {
      callApi('users/changepassword', 'POST', payload).then(res => {
        setTimeout(() => {
          setIsDisplayLoading(false);
          Swal.fire(
            'Thành công',
            'Mật khẩu của bạn đã được thay đổi',
            'success'
          );
          setPayload({});
        }, 500);
      }).catch(err => {
        setTimeout(() => {
          setIsDisplayLoading(false);
          if (err && err.response) {
            console.log(err.response.data.message);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: `${err.response.data.message}`,
            });
          }
          else {
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Có lỗi xảy ra vui lòng thử lại...',
            });
          }
        }, 500);
      });
    });
  }
  return (
    <React.Fragment>
      <div className="row change-password">
        <div className="col-sm-12">
          <strong className="changepassword-title"> Change password</strong>
          <hr />
        </div>
        <Col sm={12} md={6}>
          <Form>
            <FormGroup>
              <Label for="oldPassword">
                Old Password
              </Label>
              <Input type="password" name="oldPassword" id="oldPassword" value={payload && payload.oldPassword ? payload.oldPassword : ""} onChange={onChange} />
              {/* <FormFeedback valid>Please enter your password</FormFeedback> */}
            </FormGroup>
            <FormGroup>
              <Label for="newPassword">New Password (6 to 20 characters long)</Label>
              <Input
                type="password"
                name="newPassword"
                id="newPassword"
                onChange={onChange}
                value={payload && payload.newPassword ? payload.newPassword : ""}
              />
              {/* <FormFeedback>
                Your passwords don't match. Please try again
              </FormFeedback> */}
            </FormGroup>
          </Form>
        </Col>
        <Col sm="12">
          <hr />
          <Button className="change" onClick={onChangePassword}>Change Password</Button>
        </Col>
      </div>
    </React.Fragment>
  )
}
export default ChangePasswordForm;
