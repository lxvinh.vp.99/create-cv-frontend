import React, { useState } from 'react';
import { Link, NavLink as RRNavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Badge
} from 'reactstrap';

const Header = (props) => {
  const auth = useSelector(state => state.auth);
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const onClick = () => {

  };
  const onLogout = () => {
    localStorage.removeItem("token");
    window.location.href = "/";
  }
  return (
    <div style={{
      position: "sticky",
      top: 0,
      zIndex: 9999
    }}>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Project 3</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink tag={RRNavLink} exact to="/" activeClassName="active">Trang chủ</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={RRNavLink} exact to="/createCV" activeClassName="active">Tạo CV</NavLink>
            </NavItem>
            {auth.isLogin ?
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  {auth.userInfo && auth.userInfo.firstName && auth.userInfo.lastName ? auth.userInfo.firstName + " " + auth.userInfo.lastName : ""}
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink tag={RRNavLink} exact to="/profile/myinfo" className="active">Thông tin của tôi</NavLink>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem >
                    <NavLink onClick={onLogout} className="active">Đăng xuất</NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown> :
              <NavItem>
                <NavLink tag={RRNavLink} exact to="/login" activeClassName="active">Đăng nhập</NavLink>
              </NavItem>}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}
export default Header;
