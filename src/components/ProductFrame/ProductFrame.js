import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import callApi from '../../helpers/apiCaller';
import * as productActions from '../../actions/productActions';
import Pagination from '../Pagination/Pagination';
import ProductCard from '../ProductCard/ProductCard';
const queryString = require('query-string');
const ProductFrame = (props) => {
  let query = queryString.parse(props.location.search);
  const dispatch = useDispatch();
  const { totalProducts, products } = useSelector(state => state.products);
  let totalPage = Math.ceil(totalProducts / query.size);
  useEffect(() => {
    callApi(`products${props.location.search}`, 'POST', null).then(res => {
      dispatch(productActions.fetchProducts(res.data.data.totalProducts, res.data.data.products));
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, [props.location]);
  const onHandleNextPage = (pageindex) => {
    query.page = pageindex;
    let search = queryString.stringify(query);
    props.history.push({ search: search });
  }
  let elmProducts = [];
  if (products.length > 0) {
    elmProducts = products.map((product, index) => {
      return <ProductCard {...props} key={product._id} index={index + 1} product={product} />
    });
  }
  else {
    elmProducts = <div style={{ height: 60 + "vh", width: 100 + "%", display: "flex", alignItems: "center", justifyContent: "center" }}><h3 style={{ textAlign: "center" }}>Chưa có sản phẩm nào được đăng bán</h3></div>
  }
  return (
    <React.Fragment>
      <div className="page-wrapper">
        {elmProducts}
      </div>
      <div>
        <Pagination totalPage={totalPage} currentPage={query.page} onHandleNextPage={onHandleNextPage}></Pagination>
      </div>
    </React.Fragment>
  )
}
export default ProductFrame;
