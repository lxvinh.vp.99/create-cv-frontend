import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './style.scss';
import WishProductItem from '../WishProductItem/WishProductItem';
import callApi from '../../helpers/apiCaller';
import * as wishListActions from '../../actions/wishListActions';
const WishListFrame = (props) => {
  const dispatch = useDispatch();
  const wishList = useSelector(state => state.wishList);
  useEffect(() => {
    callApi('wishlists/getwishlist', 'POST', null).then(res => {
      dispatch(wishListActions.fetchWishList(res.data.data));
    }).catch(err => {
      console.log(err.response.data.message);
    });
  }, []);
  let elmProducts = [];
  if (wishList.length > 0) {
    elmProducts = wishList.map((product, index) => {
      return <WishProductItem key={product._id} index={index + 1} product={product} />
    });
  }
  return (
    <React.Fragment>
      <h4 style={{ marginTop: 20 + "px" }}>Danh sách sản phẩm yêu thích {"(" + wishList.length + ")"}</h4>
      <hr></hr>
      <div>
        {elmProducts}
      </div>
    </React.Fragment>
  )
}
export default WishListFrame;
