import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import callApi from '../../helpers/apiCaller';
import { toast } from 'react-toastify';
const SignUpForm = (props) => {
    const [signUpInfo, setSignUpInfo] = useState({});
    const onChange = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        setSignUpInfo({ ...signUpInfo, [name]: value });
    }
    const onSignUp = (e) => {
        e.preventDefault();
        callApi('users/create', 'POST', signUpInfo).then(res => {
            toast.dismiss();
            toast.success("Đăng ký tài khoản thành công", {
                onClose: () => {
                    props.history.push('/login', { email: signUpInfo.email, password: signUpInfo.password });
                },
                autoClose: 1000
            });
            setSignUpInfo({});
        }).catch(err => {
            toast.dismiss();
            toast.error(`${err.response.data.message}`);
        });
    }
    return (
        <React.Fragment>
            <form>
                <h3>Sign Up</h3>
                <div className="form-group">
                    <label>First name</label>
                    <input type="text"
                        className="form-control"
                        placeholder="First name"
                        name="firstName"
                        onChange={onChange}
                        value={signUpInfo && signUpInfo.firstName ? signUpInfo.firstName : ""}
                    />
                </div>
                <div className="form-group">
                    <label>Last name</label>
                    <input type="text"
                        className="form-control"
                        placeholder="Last name"
                        name="lastName"
                        onChange={onChange}
                        value={signUpInfo && signUpInfo.lastName ? signUpInfo.lastName : ""}
                    />
                </div>
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email"
                        className="form-control"
                        placeholder="Enter email"
                        name="email"
                        onChange={onChange}
                        value={signUpInfo && signUpInfo.email ? signUpInfo.email : ""}
                    />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password"
                        className="form-control"
                        placeholder="Enter password"
                        name="password"
                        onChange={onChange}
                        value={signUpInfo && signUpInfo.password ? signUpInfo.password : ""}
                    />
                </div>
                <div className="form-group">
                    <label>Confirm Password</label>
                    <input type="password"
                        className="form-control"
                        placeholder="Enter confirm password"
                        name="confirmPassword"
                        onChange={onChange}
                        value={signUpInfo && signUpInfo.confirmPassword ? signUpInfo.confirmPassword : ""}
                    />
                </div>
                <button type="submit" className="btn btn-primary btn-block" onClick={onSignUp}>Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <Link to="/login">sign in?</Link>
                </p>
            </form>
        </React.Fragment>
    )
}
export default SignUpForm;
